<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="librairievitrine.bibliothequeDAO"%>
<%@ page import="librairievitrine.Book"%>
<%@ page import="java.util.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
h1 {
	text-align: center;
}
</style>
<title>La librairie de Papy Etienne</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<div class=".container">
		<form action=${pageContext.request.contextPath}/listelivre
			method="post">

			<h1>Livres de chez eux</h1>

			<%
				ArrayList<Book> formBooks = (ArrayList) request.getAttribute("Livres");
			%>


			<%-- 		
				<table>
				<tr>
					<c:forEach items="${Livres}" var="c">
						<td>
						 <input type="submit" name="Emprunter"
							value="${c.getBooksId()}" />
							
							 <c:out value="${c.getBookTitle()}"></c:out>
							<c:out value="${c.getAuthorName()}"></c:out>
						</td>
						<br>
					</c:forEach>
					</tr>
				</table> --%>


			<table class="table">

				<thead>
					<tr>
						<th scope="col">Titre</th>
						<th scope="col">Pr�nom</th>
						<th scope="col">Nom</th>
						<th scope="col">Editeur</th>
						<th scope="col">Ann�e</th>
					</tr>
				</thead>

				<c:forEach items="${Livres}" var="c">
					<tr>
						<td><input type="submit" name="Emprunter"
							value="${c.getBooksId()}" /></td>
						<td><c:out value="${c.getBookTitle()}"></c:out></td>
						<td><c:out value="${c.getAuthorFirstName()}"></c:out>
						<td><c:out value="${c.getAuthorName()}"></c:out></td>
						<td><c:out value="${c.getEditor()}"></c:out></td>
						<td><c:out value="${c.getBookYear()}"></c:out></td>
					</tr>
				</c:forEach>

			</table>







		</form>
	</div>
</body>