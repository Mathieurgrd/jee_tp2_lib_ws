<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="librairievitrine.bibliothequeDAO"%>
<%@ page import="librairievitrine.Book"%>
<%@ page import="java.util.*"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<style type="text/css">
h1 {
	text-align: center;
	text-shadow: #000 1px 0 10px;
}
</style>
<title>Livre chez Nous</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<div class=".container">

		<form action=${pageContext.request.contextPath}/books method="post">

							<h1>Livre chez Nous</h1>

				<%
					ArrayList<Book> formBooks = (ArrayList) request.getAttribute("Livres");
				%>
				<table class="table">

					<thead class="thead-dark">
						<tr>
							<th scope="col">Titre</th>
							<th scope="col">Pr�nom</th>
							<th scope="col">Nom</th>
							<th scope="col">Editeur</th>
							<th scope="col">Ann�e</th>
						</tr>
					</thead>
					<c:forEach items="${Livres}" var="c">
						<tr>
							<td><c:out value="${c.getBookTitle()}"></c:out></td>
							<td><c:out value="${c.getAuthorFirstName()}"></c:out>
							<td><c:out value="${c.getAuthorName()}"></c:out></td>
							<td><c:out value="${c.getEditor()}"></c:out></td>
							<td><c:out value="${c.getBookYear()}"></c:out></td>
						</tr>
					</c:forEach>

				</table>


		</form>

	</div>
</body>