package librairievitrine;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.mysql.cj.jdbc.MysqlDataSource;

public class DBConnection {
	
private static Connection connection;
	
	public DBConnection () throws SQLException, IOException {
		connection = this.getCon();
	}
	
	public Connection getCon() throws SQLException, IOException {
		Properties prop = new Properties();
		FileInputStream fileInput = null;
		MysqlDataSource dataSource = null;
//		getClass().getResource("db.properties");
		fileInput = new FileInputStream(Constants.URLDB);
		prop.load(fileInput);
		dataSource = new MysqlDataSource();
		dataSource.setURL(prop.getProperty("DB_URL"));
		dataSource.setUser(prop.getProperty("DB_USERNAME"));
		dataSource.setPassword(prop.getProperty("DB_PASSWORD"));
		fileInput.close();
		connection = dataSource.getConnection();
		return connection;
	}

	public static Connection getConnection() {
		return connection;
	}

}