package librairievitrine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//Type de contrôleur spécialisé des requêtes REST.
@RestController//@RequestMapping("/v1") => Pour imposer un appel pour toutes les méthodes.
public class BookController {
	
	//Voir index.html pour l'affichage.
	@GetMapping(value="/test", produces="application/json")//Autre syntaxe pour RequestMapping (value="x", method=RequestMethod.GET)
	public Map<String, String> getHealth() {
		Map<String, String> hm = new HashMap<String, String>();
		hm.put("CookBook", "incomplete");
		hm.put("data", "OK");
		hm.put("server", "OK");
		return hm;
	}
	
	//Visualisation en JSON d'une liste de recettes.
	@GetMapping(value ="/books", produces="application/json")
	public List<Book> getBooks() throws Exception{
		bibliothequeDAO bdao = new bibliothequeDAO();
		return bdao.listerLesLivres();
		

		
	}
	
	//Fonction élégante pour afficher une recette en fonction de son numero (avec PathVariable).
	@GetMapping(produces="application/json", value="/books/{n}")//Appelle le numero de la recette.
	public Book getBook(@PathVariable("n") int n) throws Exception {
		bibliothequeDAO bdao = new bibliothequeDAO();
		return bdao.emprunterLivre(n);
	}

	

}


