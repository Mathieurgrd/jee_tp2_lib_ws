package librairievitrine;

import java.util.LinkedHashMap;

import org.springframework.web.client.RestTemplate;

public class BookClient {
	public static void main(String[] args) {
			try {
				
				RestTemplate rt = new RestTemplate();
				
				Boolean complete = rt.getForObject("http://localhost:8095/complete", Boolean.class);
				System.out.println("Complet ? " + complete);
				
				Book tarte = rt.getForObject("http://localhost:8095/receipes/0", Book.class);
				System.out.println("Recette = " + tarte.getBookTitle());
				
				System.out.println("WP = " + rt.getForObject
						("https://fr.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=tartelette", 
						LinkedHashMap.class));
				
				rt.delete("http://localhost:8095/receipes/0", Book.class);
				
			//	rt.postForLocation("http://localhost:8095/receipes", new Book("coucou", "coucou", "Cuit au feu de bois", "coucou", true));
						
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
}

