package librairievitrine;

import java.util.HashMap;
import java.util.Map;

public class BookApiGR2 {

	private int id;
	private String title;
	private String publisher;
	private int year;
	private String authorname;
	private String authorfirstname;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public BookApiGR2() {
	}

	/**
	 * 
	 * @param id
	 * @param title
	 * @param authorname
	 * @param year
	 * @param authorfirstname
	 * @param publisher
	 */
	public BookApiGR2(int id, String title, String publisher, int year, String authorname, String authorfirstname) {
		super();
		this.id = id;
		this.title = title;
		this.publisher = publisher;
		this.year = year;
		this.authorname = authorname;
		this.authorfirstname = authorfirstname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getAuthorname() {
		return authorname;
	}

	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

	public String getAuthorfirstname() {
		return authorfirstname;
	}

	public void setAuthorfirstname(String authorfirstname) {
		this.authorfirstname = authorfirstname;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "BookApiGR2 [id=" + id + ", title=" + title + ", publisher=" + publisher + ", year=" + year
				+ ", authorname=" + authorname + ", authorfirstname=" + authorfirstname + ", additionalProperties="
				+ additionalProperties + "]";
	}
	
	

}