package librairievitrine;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class bibliothequeDAO {

	private static DBConnection connection;
	private static List<Book> books;

	public List<Book> listerLesLivres() throws Exception, Exception {
		books = new ArrayList<Book>();
		connection = new DBConnection();
		PreparedStatement ps = DBConnection.getConnection().prepareStatement("SELECT * FROM livres");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int id_livre = rs.getInt(1);
			String titre = rs.getString(2);
			int annee = rs.getInt(3);
			String editeur = rs.getString(4);
			String prenom = rs.getString(5);
			String nom = rs.getString(6);
			Book book = new Book(id_livre, titre, annee, editeur, prenom, nom);
			books.add(book);
		}
		return books;

	}

	public Book emprunterLivre(int idLivre) throws Exception, Exception {
		connection = new DBConnection();
		Book book = null ;
		PreparedStatement ps = DBConnection.getConnection()
				.prepareStatement("SELECT * FROM livres WHERE livres.id_livre = " + idLivre);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			int id_livre = rs.getInt(1);
			String titre = rs.getString(2);
			int annee = rs.getInt(3);
			String editeur = rs.getString(4);
			String prenom = rs.getString(5);
			String nom = rs.getString(6);
			book = new Book(id_livre, titre, annee, editeur, prenom, nom);
		}
		PreparedStatement ps2 = DBConnection.getConnection()
				.prepareStatement("DELETE FROM livres WHERE livres.id_livre = " + idLivre);
		ps2.execute();
		return book;
	}

	public void insererLivre(String titre, int annee, String editeur, String auteurPrenom, String auteurNom)
			throws Exception, Exception {
		connection = new DBConnection();
		String sql = "INSERT INTO livres(titre, annee, editeur, prenom, nom) VALUES ('" + titre + "', '" + annee
				+ "', '" + editeur + "', '" + auteurPrenom + "', '" + auteurNom + "')";
		PreparedStatement ps = DBConnection.getConnection().prepareStatement(sql);
		ps.execute();
	}

}