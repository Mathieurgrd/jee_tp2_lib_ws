package librairievitrine;

import java.util.ArrayList;

public class Book {

	private int booksId;
	private String bookTitle;
	private int bookYear;
	private String editor;
	private String authorFirstName;
	private String authorName;

	public Book(int booksId, String bookTitle, int bookYear, String editor, String authorFirstName, String authorName) {
		this.booksId = booksId;
		this.bookTitle = bookTitle;
		this.bookYear = bookYear;
		this.editor = editor;
		this.authorFirstName = authorFirstName;
		this.authorName = authorName;
	}



//	public Book(String bookTitle, int bookYear, String editor, String authorFirstName, String authorName) {
//		super();
//		this.bookTitle = bookTitle;
//		this.bookYear = bookYear;
//		this.editor = editor;
//		this.authorFirstName = authorFirstName;
//		this.authorName = authorName;
//	}

	public int getBooksId() {
		return booksId;
	}

	public void setBooksId(int booksId) {
		this.booksId = booksId;
	}

	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public int getBookYear() {
		return bookYear;
	}

	public void setBookYear(int bookYear) {
		this.bookYear = bookYear;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public static Book converterBook(BookApiGR2 bkApi) {
		return new Book(bkApi.getId(), bkApi.getTitle(), bkApi.getYear(), bkApi.getPublisher(), bkApi.getAuthorfirstname(),
				bkApi.getAuthorname());
	}

	public static ArrayList<Book> converterBook(ArrayList<BookApiGR2> list) {
		ArrayList<Book> bookconverted = new ArrayList<>();
		for (BookApiGR2 b : list) {
			bookconverted.add(Book.converterBook(b));
		}
		return bookconverted;
	}

	@Override
	public String toString() {
		return "Book [booksId=" + booksId + ", bookTitle=" + bookTitle + ", bookYear=" + bookYear + ", editor=" + editor
				+ ", authorFirstName=" + authorFirstName + ", authorName=" + authorName + "]";
	}

}