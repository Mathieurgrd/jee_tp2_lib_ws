package librairieservlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import librairievitrine.Book;
import librairievitrine.BookApiGR2;
import librairievitrine.Constants;
import librairievitrine.bibliothequeDAO;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet(name = "/listelivre")
public class TheirBooksServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TheirBooksServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		arg1.setContentType("text/html");

		try {

			if (arg0.getParameter("Emprunter") != null) {

				System.out.println(arg0.getParameter("Emprunter"));
				empruntChezEtienne(arg0);
			}
			
			Properties prop = new Properties();
			FileInputStream fileInput = null;
			fileInput = new FileInputStream(Constants.URLPROP);
			prop.load(fileInput);
			 URL url = new URL(prop.getProperty("server.theirbooks"));
			 HttpURLConnection con = (HttpURLConnection) url.openConnection();
			 con.setRequestMethod("GET");
			 int status = con.getResponseCode();


			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			System.out.println(content);
			in.close();
			
		

			Gson gson = new Gson();
			ArrayList<BookApiGR2> listLivres = gson.fromJson(content.toString(), (new TypeToken<List<BookApiGR2>>() {
			}).getType());
			
			System.out.println(listLivres.size());

			ArrayList<Book> liste = new ArrayList<>(Book.converterBook(listLivres));
			
			liste.forEach(System.err::println);

			arg0.setAttribute("Livres", liste);

		} catch (Exception e) {
			e.printStackTrace();
		}

		this.getServletContext().getRequestDispatcher("/WEB-INF/their_books.jsp").forward(arg0, arg1);

	}

	public void empruntChezEtienne(HttpServletRequest arg0) {
		try {
			
			Properties prop = new Properties();
			FileInputStream fileInput = null;
			fileInput = new FileInputStream(Constants.URLPROP);
			prop.load(fileInput);

			URL url = new URL(prop.getProperty("server.theirbooks") + "/" + arg0.getParameter("Emprunter"));
			System.out.println(url);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			int status = con.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			System.out.println(content);
			in.close();

			Gson gson = new Gson();
			BookApiGR2 bouquin = gson.fromJson(content.toString(), BookApiGR2.class);

			Book b = Book.converterBook(bouquin);

			bibliothequeDAO bdao = new bibliothequeDAO();
			bdao.insererLivre(b.getBookTitle(), b.getBookYear(), b.getEditor(), b.getAuthorFirstName(),
					b.getAuthorName());

//				arg0.setAttribute("livre", b);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** TEST AVEC JSON EN DUR */
	public void empruntChezEtienne(int id) {
		try {

			File file = new File("C:\\Eclipse\\librairie-project_2\\librairie_vitrine\\main\\resources\\test_json.txt");
			BufferedReader br = new BufferedReader(new FileReader(file)); 

//			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();

//			while ((inputLine = in.readLine()) != null) {
//				content.append(inputLine);
//			}
//			System.out.println(content);
//			in.close();
			
			while ((inputLine = br.readLine()) != null) {
				content.append(inputLine);
			}
			System.out.println(content);
			br.close();

			Gson gson = new Gson();
			ArrayList<BookApiGR2> listLivres = gson.fromJson(content.toString(), (new TypeToken<List<BookApiGR2>>() {
			}).getType());
			
			System.out.println(listLivres.size());

			ArrayList<Book> liste = new ArrayList<>(Book.converterBook(listLivres));
			
			liste.forEach(System.err::println);

			Book b = null; 
			
			for(Book mldksl : liste) {
				
				if(mldksl.getBooksId() == id) {
					b = mldksl;
				}
			}
			 

			bibliothequeDAO bdao = new bibliothequeDAO();
			bdao.insererLivre(b.getBookTitle(), b.getBookYear(), b.getEditor(), b.getAuthorFirstName(),
					b.getAuthorName());

//				arg0.setAttribute("livre", b);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	


}
