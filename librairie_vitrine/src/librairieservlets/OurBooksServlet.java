package librairieservlets;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mysql.cj.jdbc.MysqlDataSource;

import librairievitrine.Book;
import librairievitrine.BookApiGR2;
import librairievitrine.Constants;
import librairievitrine.bibliothequeDAO;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/books")
public class OurBooksServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OurBooksServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		arg1.setContentType("text/html");

		try {
		
			Properties prop = new Properties();
			FileInputStream fileInput = null;
//			getClass().getResource("db.properties");
			fileInput = new FileInputStream(Constants.URLPROP);
			prop.load(fileInput);

			URL url = new URL(prop.getProperty("server.ourbooks"));
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			int status = con.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();

			Gson gson = new Gson();
			ArrayList<Book> listLivres = gson.fromJson(content.toString(), (new TypeToken<List<Book>>() {
			}).getType());

			arg0.setAttribute("Livres", listLivres);

		} catch (Exception e) {
			e.printStackTrace();
		}

		this.getServletContext().getRequestDispatcher("/WEB-INF/our_books.jsp").forward(arg0, arg1);
	}


	

}
