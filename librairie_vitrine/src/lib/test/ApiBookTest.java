package lib.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import librairievitrine.Book;
import librairievitrine.bibliothequeDAO;

public class ApiBookTest {

	bibliothequeDAO bdao = new bibliothequeDAO();

	@Test
	public void testApiCall() throws Exception {

		ArrayList<Book> arList = new ArrayList<>();

		URL url = new URL("http://localhost:8095/books");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		int status = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		System.out.println(content);
		in.close();

		Gson gson = new Gson();
		arList = gson.fromJson(content.toString(), (new TypeToken<List<Book>>() {
		}).getType());

		arList.forEach(System.err::println);
		assertEquals((ArrayList<Book>)bdao.listerLesLivres(), arList);
	}

}
